from keras.layers import Conv2D, Embedding, SpatialDropout1D, LSTM
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.optimizers import SGD
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from sklearn.model_selection import train_test_split
from matplotlib import pyplot
from keras.datasets import cifar10
from keras.utils import to_categorical
from keras.models import Sequential
import re
import numpy as np
import pandas as pd

max_features = 2000
embed_dim = 128
lstm_out = 196
batch_size = 32

attributes = ['target', 'id', 'date', 'flag', 'user', 'text']


def build_model(frame):
    model = Sequential()
    model.add(Embedding(max_features, embed_dim, input_length=frame.shape[1]))
    model.add(SpatialDropout1D(0.4))
    model.add(LSTM(lstm_out, dropout=0.2, recurrent_dropout=0.2))
    model.add(Dense(2, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model


def clear_data_frame(frame):
    frame = frame[['target', 'text']]
    frame['text'] = frame['text'].apply(lambda x: x.lower())
    frame['text'] = frame['text'].apply((lambda x: re.sub('[^a-zA-z0-9\s]', '', x)))

    frame['target'] = frame['target'].astype(int)
    return frame


data = pd.read_csv('./dataset/training.csv', names=attributes, engine='python')
data = data.sample(frac=1, axis=1).reset_index(drop=True)
drop_indices = np.random.choice(data.index, 1500000, replace=False)
df_subset = data.drop(drop_indices)

train_data = clear_data_frame(df_subset)

tokenizer = Tokenizer(num_words=max_features, split=' ')
tokenizer.fit_on_texts(train_data['text'].values)
X = tokenizer.texts_to_sequences(train_data['text'].values)
X = pad_sequences(X)

Y = pd.get_dummies(train_data['target']).values

X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.33, random_state=42)

model = build_model(X)

model.fit(X_train, y_train, epochs=7, batch_size=batch_size, verbose=2)

validation_size = 1500

X_validate = X_test[-validation_size:]
y_validate = y_test[-validation_size:]
X_test = X_test[:-validation_size]
y_test = y_test[:-validation_size]

score, accuracy = model.evaluate(X_test, y_test, verbose=2, batch_size=batch_size)

pos_cnt, neg_cnt, pos_correct, neg_correct = 0, 0, 0, 0
for x in range(len(X_validate)):

    result = model.predict(X_validate[x].reshape(1, X_test.shape[1]), batch_size=1, verbose=2)[0]

    if np.argmax(result) == np.argmax(y_validate[x]):
        if np.argmax(y_validate[x]) == 0:
            neg_correct += 1
        else:
            pos_correct += 1

    if np.argmax(y_validate[x]) == 0:
        neg_cnt += 1
    else:
        pos_cnt += 1

print("pos_acc", pos_correct / pos_cnt * 100, "%")
print("neg_acc", neg_correct / neg_cnt * 100, "%")

twt = ['Meetings: Because none of us is as dumb as all of us.']
# vectorizing the tweet by the pre-fitted tokenizer instance
twt = tokenizer.texts_to_sequences(twt)
# padding the tweet to have exactly the same shape as `embedding_2` input
twt = pad_sequences(twt, maxlen=32, dtype='int32', value=0)
print(twt)
sentiment = model.predict(twt, batch_size=1, verbose=2)[0]
if (np.argmax(sentiment) == 0):
    print("negative")
elif (np.argmax(sentiment) == 1):
    print("positive")
