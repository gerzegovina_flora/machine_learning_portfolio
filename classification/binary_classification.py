import pandas as pd
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
import matplotlib.pyplot as plt

data = pd.read_csv('./dataset/titanic.csv', delimiter=',')
data = data.drop(['Name'], axis=1)
data = data.replace('male', 1)
data = data.replace('female', 0)
data = data.astype(float)

X = data.iloc[:, 1:]
y = data.iloc[:, 0]

X_train, X_test, y_train, y_test = train_test_split(X, y)


def show_performance_of_classifier(classifier):
    classifier.fit(X_train, y_train)
    predicted = classifier.predict(X_test)

    print(metrics.accuracy_score(predicted, y_test))
    print(metrics.roc_auc_score(predicted, y_test))

    fpr, tpr, _ = metrics.roc_curve(y_test, predicted)
    auc = metrics.roc_auc_score(y_test, predicted)
    plt.title("Mode: " + type(classifier).__name__)
    plt.plot(fpr, tpr, label="data 1, auc=" + str(auc))
    plt.legend(loc=4)
    plt.show()


show_performance_of_classifier(LogisticRegression())
show_performance_of_classifier(GaussianNB())
show_performance_of_classifier(KNeighborsClassifier())
show_performance_of_classifier(SVC())
show_performance_of_classifier(DecisionTreeClassifier())
