import numpy as np
import pandas as pd
import seaborn as sn
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler
from scipy import stats
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LinearRegression
import seaborn as sns

from classification import util


def show_performance_of_classifier(classifier):
    classifier.fit(X_train, y_train)
    predicted = classifier.predict(X_test)

    print(accuracy_score(predicted, y_test))
    plt.scatter(predicted, y_test, color='black')
    plt.plot(predicted, predicted, color='red', linewidth=2)
    plt.ylabel('Actual Age')
    plt.xlabel('Predicted Age')
    plt.show()


def adjust_features(X):
    scaler = MinMaxScaler()
    X = scaler.fit_transform(X)

    pca = PCA()
    pca.fit(X)

    plt.figure()
    plt.plot(np.cumsum(pca.explained_variance_ratio_))
    plt.xlabel('Number of Components')
    plt.ylabel('Variance (%)')
    plt.show()

    pca = PCA(n_components=4)
    return pca.fit_transform(X)


data = util.get_data_frame()
data.hist(figsize=(20, 10), grid=False, layout=(3, 3), bins=30)
plt.show()

sn.heatmap(data.corr())
plt.show()

X, y = util.get_X_and_y()
X = adjust_features(X)

X_train, X_test, y_train, y_test = train_test_split(X, y)

nf = data.select_dtypes(include=[np.number]).columns

skew_list = stats.skew(data[nf])
skew_list_df = pd.concat([pd.DataFrame(nf, columns=['Features']), pd.DataFrame(skew_list, columns=['Skewness'])],
                         axis=1)
skew_list_df.sort_values(by='Skewness', ascending=False)

sns.set()
cols = [
    'sex', 'length', 'diameter', 'height', 'whole_weight', 'shucked_weight', 'viscera_weigh',
    'shell_weigh', 'rings'
]
sns.pairplot(data[cols], height=2.5)
plt.show()

data = pd.concat([data['rings'], data['sex']], axis=1)
f, ax = plt.subplots(figsize=(8, 6))
fig = sns.boxenplot(x='sex', y="rings", data=data)
fig.axis(ymin=0, ymax=30)
plt.show()

show_performance_of_classifier(DecisionTreeClassifier())
show_performance_of_classifier(KNeighborsClassifier())
show_performance_of_classifier(RandomForestClassifier())

paramLin = {'fit_intercept': [True, False], 'normalize': [True, False], 'copy_X': [True, False]}
LinearReg = GridSearchCV(LinearRegression(), paramLin, cv=10)
LinearReg.fit(X=X_train, y=y_train)
LinearRegmodel = LinearReg.best_estimator_
print(LinearReg.best_score_, LinearReg.best_params_)

tree_para = {'criterion': ['gini', 'entropy'],
             'max_depth': [4, 5, 6, 7, 8, 9, 10, 11, 12, 15, 20, 30, 40, 50, 70, 90, 120, 150]}
LinearReg = GridSearchCV(DecisionTreeClassifier(), tree_para, cv=10)
LinearReg.fit(X=X_train, y=y_train)
LinearRegmodel = LinearReg.best_estimator_
print(LinearReg.best_score_, LinearReg.best_params_)

parameters = {
    'n_estimators': [320, 330, 340],
    'max_depth': [8, 9, 10, 11, 12],
    'random_state': [0],
    # 'max_features': ['auto'],
    'criterion' :['gini', 'entropy'],
}
LinearReg = GridSearchCV(RandomForestClassifier(), tree_para, cv=10)
LinearReg.fit(X=X_train, y=y_train)
LinearRegmodel = LinearReg.best_estimator_
print(LinearReg.best_score_, LinearReg.best_params_)

Cs = [0.001, 0.01, 0.1, 1, 10]
gammas = [0.001, 0.01, 0.1, 1]
param_grid = {'C': Cs, 'gamma' : gammas}
LinearReg = GridSearchCV(SVC(kernel='rbf'), param_grid, cv=10)
LinearReg.fit(X=X_train, y=y_train)
LinearRegmodel = LinearReg.best_estimator_
print(LinearReg.best_score_, LinearReg.best_params_)

k_range = list(range(1,31))
weight_options = ["uniform", "distance"]

param_grid = dict(n_neighbors = k_range, weights = weight_options)
LinearReg = GridSearchCV(KNeighborsClassifier(n_neighbors=13), param_grid, cv=10, scoring='accuracy')
LinearReg.fit(X=X_train, y=y_train)
LinearRegmodel = LinearReg.best_estimator_
print(LinearReg.best_score_, LinearReg.best_params_)

