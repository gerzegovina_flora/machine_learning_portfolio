from sklearn.linear_model import LogisticRegression
from classification import util
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import VotingClassifier

X, y = util.get_X_and_y()
X_train, X_test, y_train, y_test = util.get_data()

logistic_regression_classifier = LogisticRegression()
knn_classifier = LogisticRegression()
decision_tree_regressor_classifier = DecisionTreeClassifier()

classifier = VotingClassifier(estimators=[
    ('lr', logistic_regression_classifier),
    ('knn', knn_classifier),
    ('dtrc', decision_tree_regressor_classifier)
], voting='hard')

classifier = classifier.fit(X_train, y_train)

predicted_values = classifier.predict(X_test)

util.scatter(y_test, predicted_values)
util.show_score(classifier, X, y)
