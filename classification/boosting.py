from classification import util
import numpy as np
from sklearn.ensemble import GradientBoostingClassifier
from xgboost import XGBClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import seaborn as sn
from classification import util
from sklearn.decomposition import PCA
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler


def show_performance_of_classifier(classifier):
    classifier.fit(X_train, y_train)
    predicted = classifier.predict(X_test)

    print(accuracy_score(predicted, y_test))
    plt.scatter(predicted, y_test, color='black')
    plt.plot(predicted, predicted, color='red', linewidth=2)
    plt.ylabel('Actual Age')
    plt.xlabel('Predicted Age')
    plt.show()


def adjust_features(X):
    scaler = MinMaxScaler()
    X = scaler.fit_transform(X)

    pca = PCA()
    pca.fit(X)

    plt.figure()
    plt.plot(np.cumsum(pca.explained_variance_ratio_))
    plt.xlabel('Number of Components')
    plt.ylabel('Variance (%)')
    plt.show()

    pca = PCA(n_components=4)
    return pca.fit_transform(X)


data = util.get_data_frame()
data.hist(figsize=(20, 10), grid=False, layout=(3, 3), bins=30)
plt.show()

sn.heatmap(data.corr())
plt.show()

X, y = util.get_X_and_y()
X = adjust_features(X)

X_train, X_test, y_train, y_test = train_test_split(X, y)

lr_list = [0.01, 0.05, 0.075, 0.1, 0.25, 0.5, 0.75, 1]

for learning_rate in lr_list:
    gb_clf = GradientBoostingClassifier(n_estimators=100, learning_rate=learning_rate, max_features=2, max_depth=2,
                                        random_state=0)
    gb_clf.fit(X_train, y_train)

    print("Learning rate: ", learning_rate)
    print("Accuracy score (training): {0:.3f}".format(gb_clf.score(X_train, y_train)))
    print("Accuracy score (validation): {0:.3f}".format(gb_clf.score(X_test, y_test)))

xgb_clf = XGBClassifier()
xgb_clf.fit(X_train, y_train)
score = xgb_clf.score(X_test, y_test)
print(score)
