import pandas as pd
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score

attributes = [
    'sex', 'length', 'diameter', 'height', 'whole_weight', 'shucked_weight', 'viscera_weigh',
    'shell_weigh', 'rings'
]

data = pd.read_csv('./dataset/abalone.data', names=attributes)
data = data.replace({
    'F': 0,
    'M': 1,
    'I': 2,
}, regex=True)
data = data.astype(float)

X = data.iloc[:, :-1]
y = data.iloc[:, -1]


def get_X_and_y():
    return X, y


def get_data_frame():
    return data


def get_data():
    return train_test_split(X, y)


def show_score(clf, X, y):
    print(cross_val_score(clf, X, y, cv=3).mean())


def scatter(test, predicted):
    fig, ax = plt.subplots(figsize=(8, 8))
    ax.plot(test, predicted, '.k')

    ax.plot([0, 30], [0, 30], '--k')
    ax.plot([0, 30], [2, 32], ':k')
    ax.plot([2, 32], [0, 30], ':k')

    rms = (test - predicted).std()

    ax.text(25, 3,
            "Root Mean Square Error = %.2g" % rms,
            ha='right', va='bottom')

    ax.set_xlim(0, 30)
    ax.set_ylim(0, 30)

    ax.set_xlabel('True number of rings')
    ax.set_ylabel('Predicted number of rings')

    plt.show()
    return rms
