from classification import util
from sklearn.ensemble import BaggingClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import DecisionTreeRegressor

X, y = util.get_X_and_y()
X_train, X_test, y_train, y_test = util.get_data()


# shows the best result
def train_using_decision_tree():
    classifier = DecisionTreeRegressor(max_depth=10)
    classifier.fit(X_train, y_train)

    predicted_values = classifier.predict(X_test)

    util.scatter(y_test, predicted_values)
    util.show_score(classifier, X, y)


def train_using_bagging_with_decision_tree(classification_algorithm):
    classifier = classification_algorithm
    classifier.fit(X_train, y_train)

    predicted_values = classifier.predict(X_test)

    util.scatter(y_test, predicted_values)
    util.show_score(classifier, X, y)


train_using_decision_tree()
train_using_bagging_with_decision_tree(BaggingClassifier(DecisionTreeClassifier(), n_estimators=400))
train_using_bagging_with_decision_tree(
    RandomForestRegressor(n_estimators=10, max_depth=None, min_samples_split=2, random_state=0))
