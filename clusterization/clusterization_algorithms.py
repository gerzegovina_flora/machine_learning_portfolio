import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets
from sklearn.cluster import KMeans, SpectralClustering, AgglomerativeClustering

n_samples = 1500
circles, circles_clusters = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
moons, moon_clusters = datasets.make_moons(n_samples=n_samples, noise=.05)
blobs, blob_clusters = datasets.make_blobs(n_samples=n_samples, random_state=8)
no_structure = np.random.rand(n_samples, 2)

data_set = [circles, moons, blobs, no_structure]


def kNN():
    for data in data_set:
        clustering = KMeans(n_clusters=2, random_state=0)
        labels = clustering.fit_predict(data)
        plt.scatter(data[:, 0], data[:, 1], s=15, linewidth=0, c=labels, cmap='flag')
        plt.show()


def hierarchical():
    for data in data_set:
        clustering = AgglomerativeClustering(n_clusters=2, linkage='ward')
        labels = clustering.fit_predict(data)
        plt.scatter(data[:, 0], data[:, 1], s=15, linewidth=0, c=labels, cmap='flag')
        plt.show()


def spectral():
    for data in data_set:
        clustering = SpectralClustering(n_clusters=2, eigen_solver='arpack', affinity="nearest_neighbors")
        labels = clustering.fit_predict(data)
        plt.scatter(data[:, 0], data[:, 1], s=15, linewidth=0, c=labels, cmap='flag')
        plt.show()


# kNN()
# hierarchical()
spectral()
